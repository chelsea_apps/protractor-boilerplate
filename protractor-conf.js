'use strict';

var express = require('express');
var http = require('http');
var app = express();
var testServPort = 8101;

//Use the www folder
app.use(express.static('www'));
//Use the screengrab server folder
//app.use('server', express.static('test/e2e/screengrabs/server'));

var browsername = process.env.browser || 'chrome';

var serverRunning = false;
var server;

exports.config = {

    seleniumServerJar: 'node_modules/protractor/selenium/selenium-server-standalone-2.52.0.jar',

    capabilities: {
        'browserName': browsername,
        'phantom.binary.path': 'node_modules/phantomjs/lib/phantom/bin/phantomjs',
        'chromeOptions': {
            args: ['--no-sandbox']
        }
    },

    chromeOnly: true,

    suites: {
        screengrabs: 'test/e2e/screengrabs/screengrabs.spec.js',
        login: 'test/e2e/login/*.spec.js',
        termsofuse: 'test/e2e/termsofuse/*.spec.js',
        password: 'test/e2e/password/*.spec.js',
        moremenu: 'test/e2e/moremenu/*.spec.js',
        blog: 'test/e2e/blog/*.spec.js',
        retirement: 'test/e2e/retirement/*.spec.js',
        landingpage: 'test/e2e/landing/*.spec.js',
        chooseplan: 'test/e2e/chooseplan/*.spec.js',
        fundlist: 'test/e2e/fundlist/*.spec.js',
        portfoliodetails: 'test/e2e/portfoliodetails/*.spec.js',
        cashholdings: 'test/e2e/cashholdings/*.spec.js',
        recentpayments: 'test/e2e/recentpayments/*.spec.js',
        regularpayments: 'test/e2e/regularpayments/*.spec.js',
        directdebit: 'test/e2e/directdebitsandinvestments/*.spec.js',
        funddetails: 'test/e2e/funddetails/*.spec.js',
        topup: 'test/e2e/topup/*.spec.js',
        confirmpayments: 'test/e2e/confirmpayments/*.spec.js',
        termsandconditions: 'test/e2e/termsandconditions/*.spec.js',
        paymentsubmitted: 'test/e2e/paymentsubmitted/*.spec.js',
        miscellaneous: 'test/e2e/miscellaneous/*.spec.js',
        importantinfo: 'test/e2e/importantinfo/*.spec.js'
    },

    //relative to conf directory
    // specs: [
    //     'test/e2e/**/*.spec.js'
    //   ],

    baseUrl: 'http://localhost:' + testServPort + '/',

    params: {
        baseUrl: 'http://localhost:' + testServPort + '/'
    },

    framework: 'jasmine2',

    onPrepare: function() {

        var disableNgSwipe = function() {
            angular
                .module('disableNgSwipe', [])
                .run(['$ionicConfig', function($ionicConfig) {
                    $ionicConfig.views.transition('none');
                }]);
        };

        var disableNgAnimate = function() {
            angular
                .module('disableNgAnimate', [])
                .run(['$animate', function($animate) {
                    $animate.enabled(false);
                }]);
        };

        var disableCssAnimate = function() {
            angular
                .module('disableCssAnimate', [])
                .run(function() {
                    var style = document.createElement('style');
                    style.type = 'text/css';
                    style.innerHTML = '* {' +
                        '-webkit-transition: none !important;' +
                        '-moz-transition: none !important' +
                        '-o-transition: none !important' +
                        '-ms-transition: none !important' +
                        'transition: none !important' +
                        '}' +
                        '.spinner-container {z-index:-999 !important; height: 0 !important; width:0 !important;display:none !important}';
                    document.getElementsByTagName('head')[0].appendChild(style);
                });
        };

        browser.addMockModule('disableNgAnimate', disableNgAnimate);
        browser.addMockModule('disableCssAnimate', disableCssAnimate);
        browser.addMockModule('disableNgSwipe', disableNgSwipe);

        //Start a server
        if(!serverRunning) {
            server = http.createServer(app).listen(testServPort);            
        }

        var jasmineReporters = require('jasmine-reporters');
        var SpecReporter = require('jasmine-spec-reporter');
        var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

        jasmine.getEnv().addReporter(new SpecReporter({ displayStacktrace: 'all' }));

        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: 'test/test-results',
            filePrefix: 'xmloutput'
        }));

        jasmine.getEnv().addReporter(
            new Jasmine2HtmlReporter({
                savePath: 'test-reports/',
                takeScreenshotsOnlyOnFailures: false,
                screenshotsFolder: 'images',
                filePrefix: 'index',
                fixedScreenshotName: true
            })
        );

        // jasmine.getEnv().addReporter(new jasmineReporters.TerminalReporter({
        //     verbosity: 3,
        //     color: true,
        //     showStack: false
        // }));

        browser.driver.manage().window().setSize(340, 800);

    },

    afterComplete: function() {
        server.close();
    },

    getPageTimeout: 500000,
    allScriptsTimeout: 600000,

    jasmineNodeOpts: {
        print: function() {},
        // If true, print colors to the terminal.
        showColors: true,
        // Default time to wait in ms before a test fails.
        defaultTimeoutInterval: 500000,
        isVerbose: false,
        includeStackTrace: false,
        realtimeFailure: true
    },

    singleRun: true

};
