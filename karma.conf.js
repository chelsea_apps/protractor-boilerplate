'use strict';

module.exports = function(config) {
    config.set({
        basePath: './',
        frameworks: ['jasmine-jquery', 'jasmine'],
        files: [
            'www/lib/ionic/js/ionic.bundle.min.js',
            'www/lib/ionic/js/ionic-angular.js',
            'www/lib/angular-messages/angular-messages.js',
            'www/lib/angular-toastr/dist/angular-toastr.tpls.min.js',
            'www/lib/angular-cookies/angular-cookies.js',
            'www/lib/ng-focus-if/focusif.js',
            'www/lib/spin.js/spin.js',
            'www/lib/angular-spinner/angular-spinner.js',
            'www/lib/lodash/lodash.js',
            'www/lib/angular-ui-router/release/angular-ui-router.js',
            'www/lib/angularjs-ordinal-filter/ordinal-browser.js',
            'www/lib/chartist/dist/chartist.min.js',
            'www/lib/angular-chartist.js/dist/angular-chartist.min.js',
            'www/lib/ng-pdfviewer/ng-pdfviewer.js',
            'www/lib/ngCordova/dist/ng-cordova.js',
            'www/lib/angular-mocks/angular-mocks.js',
            'www/lib/ng-pdfviewer/lib/pdf.js',
            'www/lib/ng-pdfviewer/ng-pdfviewer.js',
            'www/lib/moment/moment.js',
            'www/lib/angular-moment/angular-moment.js',
            'www/templates/*.js',
            'www/components/login/login.directives.js',
            'www/components/login/directives/*.js',
            'www/components/login/login.config.js',
            'www/components/login/forgotten-login-details.directive.js',
            'www/components/login/*.js',
            'www/components/login/*.spec.js',
            'www/components/utility/*.js',
            'www/components/core/*.js',
            'www/components/styleguide/*.js'
        ],

        singleRun: false,
        client: {
            captureConsole: true
        },
        restartOnFileChange: true,
        autoWatch: false,
        reporters: ['dots', 'coverage'],
        port: 8080,
        logLevel: config.LOG_ERROR,
        preprocessors: {
            // Update this if you change the yeoman config path
            'NOwww/components/**/*.js': ['coverage']
        },

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome','Safari'],

        plugins: [
            'karma-chrome-launcher',
            'karma-coverage',
            'karma-jasmine-jquery',
            'karma-jasmine'
        ],

        coverageReporter: {
            reporters: [{
                type: 'html',
                dir: 'coverage/'
            }, {
                type: 'text-summary'
            }]
        }
    });
};
