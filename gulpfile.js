var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var wiredep = require('wiredep').stream;
var ngConstant = require('gulp-ng-constant');
var asciify = require('asciify');
var inject = require('gulp-inject');
var jshint = require('gulp-jshint');
var ngAnnotate = require('gulp-ng-annotate');
var templateCache = require('gulp-angular-templatecache');
var useref = require('gulp-useref');
var args = require('yargs').argv;
var path = require('path');
var karma = require('karma');
var karmaParseConfig = require('karma/lib/config').parseConfig;
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var gls = require('gulp-live-server');
var gulpCopy = require('gulp-copy');
var runSequence = require('run-sequence');
var include = require("gulp-include");
var sourcemaps = require('gulp-sourcemaps');
var setTerminalTitle = require('set-terminal-title');
var lazypipe = require('lazypipe');
var del = require('del');
var jscs = require('gulp-jscs');
var noop = function() {};
var stylish = require('gulp-jscs-stylish');
const cached = require('gulp-cached');
var envConfig;

function runKarma(configFilePath, options, cb) {

    configFilePath = path.resolve(configFilePath);

    var server = karma.server;
    var log = gutil.log,
        colors = gutil.colors;
    var config = karmaParseConfig(configFilePath, {});

    Object.keys(options).forEach(function(key) {
        config[key] = options[key];
    });

    server.start(config, function(exitCode) {
        log('Karma has exited with ' + colors.red(exitCode));
        cb();
        process.exit(exitCode);
    });
}

var asciiFont = 'basic';
var paths = {
    android: './platforms/android/assets/www',
    dist: './.tmp',
    src: './www',
    sass: ['./scss/**/*.scss', './www/components/**/*.scss'],
    templateCache: ['./www/components/**/*.html'],
    ngannotate: ['./www/components/**/*.js'],
    useref: ['./www/components/**/*.js'],
    js: ['./www/components/**/*.js']
};

gulp.task('default', function() {
    runSequence(
        ['config'], ['constants'], ['index'], ['sass', 'templatecache'], ['useref'], ['uglify', 'ngannotate']
    );
});

gulp.task('fakeServer', function() {
    runSequence(
        ['config'], ['fakeServerChild']
    );
});

gulp.task('jscs', function() {
    gulp.src(['www/components/**/*.js', '!www/backend/*.js', '!www/components/utility/ngConstants.js'])
        .pipe(cached('linting'))
        .pipe(jshint()) // hint (optional) 
        .pipe(jscs({ fix: false })) // enforce style guide 
        .pipe(stylish.combineWithHintResults()) // combine with jshint results 
        .pipe(jshint.reporter('jshint-stylish')); // use any jshint reporter to log hint 
    // and style guide errors 
});

/** single run */
gulp.task('test', function(cb) {
    runKarma('karma.conf.js', {
        autoWatch: true,
        singleRun: false
    }, cb);
});

/** continuous ... using karma to watch (feel free to circumvent that;) */
gulp.task('test-dev', function(cb) {
    runKarma('karma.conf.js', {
        autoWatch: true,
        singleRun: false
    }, cb);
});



gulp.task('build', function() {
    return runSequence(
        ['sass'], ['useref'], ['templatecache'], ['clean']
    );
});


gulp.task('clean', function() {
    return del([
        paths.android + '/components',
    ]);
});

gulp.task('useref', function() {
    return gulp.src(paths.src + '/index.html')
        .pipe(useref({}, lazypipe().pipe(sourcemaps.init, {
            loadMaps: true
        })))
        .pipe(gulpif('*.js', ngAnnotate({
            single_quotes: true
        })))
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(paths.android));
});

gulp.task('ngannotate', function(done) {
    gulp.src([paths.dist + '/scripts/components.js'])
        .pipe(ngAnnotate({
            single_quotes: true
        }))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist + '/scripts'))
        .on('end', done);
});

gulp.task('uglify', function(done) {
    gulp.src([paths.dist + '/scripts/vendor.js'])
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist + '/scripts'))
        .on('end', done);
});


gulp.task('templatecache', function(done) {
    gulp.src('./www/components/**/*.html')
        .pipe(templateCache({
            standalone: true,
            single_quotes: true
        }))
        .pipe(gulp.dest('./www/templates/'))
        .on('end', done);
});


gulp.task('index', function() {

    getConfig();

    var js;

    if (envConfig.ENV.fakeServer) {
        js = ['./www/components/app/app.js','./www/components/**/*config.js','./www/components/login/*.js', './www/components/**/*transformer.js', './www/components/**/*.js', '!./www/components/**/*spec.js', envConfig.ENV.fakeServer];
    } else {
        js = ['./www/components/app/app.js','./www/components/**/*config.js','./www/components/login/*.js', './www/components/**/*transformer.js', './www/components/**/*.js', '!./www/components/backend/**/*.js', '!./www/components/**/*spec.js'];
    }
    var sources = gulp.src(js);

    return gulp.src('./www/index.html')
        .pipe(inject(
            sources, { relative: true }))
        .pipe(gulp.dest('./www'));
});


gulp.task('sass', function(done) {
    gulp.src('./scss/ionic.app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./www/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./www/css/'))
        .on('end', done);
});

gulp.task('config', function() {
    getConfig();
})

function getConfig() {
    var myConfig = require('./environments.json');
    var env = process.env.environment || 'development'
    envConfig = myConfig[env];

    console.log('**', envConfig);
}

gulp.task('constants', function(done) {

    var colour = envConfig.colour;

    setTerminalTitle('Serving ' + envConfig.ENV.name);

    asciify(envConfig.ENV.name, {
        font: asciiFont,
        color: colour
    }, function(err, res) {
        console.log(res)
    });

    return ngConstant({
            name: 'config',
            constants: envConfig,
            stream: true,
            wrap: '/* jshint ignore: start */ \'use strict\';\n<%= __ngModule %>',

        })
        .pipe(gulp.dest('www/components/utility'));
});

gulp.task('lint', function() {
    return gulp.src('./www/components/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('bower', function(done) {
    gulp.src('./www/index.html')
        .pipe(wiredep({
            exclude: ["lib/ionic/release/js/ionic-angular.js", "lib/ionic/release/js/ionic.js", "lib/ionic/css/ionic.css", "lib/angular-ui-router/release/angular-ui-router.js", "lib/angular-sanitize/angular-sanitize.js", "www/lib/angular/angular.js", "lib/angular-animate/angular-animate.js", "lib/ionic/js/ionic-angular.js", "lib/ionic/js/ionic.js"]
        }))
        .pipe(gulp.dest('./www'))
        .on('end', done);
});

gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.js, ['jscs']);
    gulp.watch(paths.templateCache, ['templatecache']);
});

gulp.task('install', ['git-check'], function() {
    return bower.commands.install()
        .on('log', function(data) {
            gutil.log('bower', gutil.colors.cyan(data.id), data.message);
        });
});

gulp.task('git-check', function(done) {
    if (!sh.which('git')) {
        console.log(
            '  ' + gutil.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required to download Ionic.',
            '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});
