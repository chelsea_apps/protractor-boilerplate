'use strict';
var Jimp = require('jimp');
var fs = require('fs');
var path = require('path');
var mkdirp = require('mkdirp');
var sourceFolder = './test/screenshots';
var outputFolder = './test/screenshots/framed/';
var outputPrefix = 'framed-';
var outputSuffix = '';

function isImage(file) {
    var res = file.indexOf('.png') >= 1;
    return res;
}

mkdirp(outputFolder, function(err) {
    if (err) {
        console.error(err);
    } else {
        console.log('Framed folder created');
    }
});

Jimp.read('nexus5.png', function(err, nexus5) {
    // do stuff with the image (if no exception)
    fs.readdir(sourceFolder, function(err, files) {

        files = files.filter(isImage);

        for (var i = 0, x = files.length; i < x; i++) {
            var sourceFilePath = sourceFolder + '/' + files[i];
            var sourceFilename = path.basename(files[i]);
            doImage(sourceFilePath, sourceFilename, nexus5);
        }

    });
});

function doImage(sourceFilePath, sourceFilename, nexus5) {
    Jimp.read(sourceFilePath, function(err, screenshot) {
        screenshot.resize(400, Jimp.AUTO);
        if (err) {
            throw err;
        }
        var outputFileName = outputFolder + outputPrefix + sourceFilename.replace('.png', outputSuffix + '.png');
        console.log('Saving to ' + outputFileName);
        //        nexus5.resize(1258, 2180) //size to our screenshots
        nexus5.resize(629, Jimp.AUTO) //size to our screenshots
            //            .composite(screenshot, 226, 418) //merge them together with nexus 5 in the background
            .composite(screenshot, 113, 209) //merge them together with nexus 5 in the background
            .write(outputFileName);
    });
}
