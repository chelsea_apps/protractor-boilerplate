# Protractor Boilerplate
>It's really simple.

##Pre-requisites
Software versions:

```Shell
$ grunt -version
  grunt-cli v0.1.13
  grunt v0.4.5

$ node -v
  v5.1.0

$ npm -v
  3.5.3
```

##Install
You will need Homebrew to get started.
To install Homebrew see: http://brew.sh/

Then some setup.
```Shell
brew update

brew doctor

export PATH="/usr/local/bin:$PATH"

brew install node

npm install -g grunt-cli
```

Then install protractor globally.
```Shell
To install protractor
npm install -g protractor
```

To update the webdriver
```Shell
webdriver-manager update
```

Some reading is suggested.
```Shell
http://www.protractortest.org/#/
```

##Understanding how this all works.
Ok, so in your node project you will see some files and folders. Most of it is project code, the not so fun stuff.
In the main project folder you will make three things:
```Shell
protractor-conf.js

package.json

mkdir test/e2e
```

Protractor-conf is where all the beautiful set up is going to be done. Like setting the browser size and your onPrepare statment which is like your 'before do'.

Package.json we will use for some suits (more on that later).

And our 'test' folder which has another 'e2e' folder. 'e2e' is where our tests scripts will live. Other tests such as Unit test will also live in the 'test' folder.

##In the e2e folder
So to get started you will see a 'common' folder and a 'login' folder.
Also a 'journeys' and 'screengrabs' folders which has some other cool code for other things for making the Screenshot pdfs. (not relevent to protractor but still cool)

Common has a globals file which will house all your reusable methods.

Login has the format you will want to follow!
You will make a folder for each page in your application. Each folder will have a .spec.js file and a .page.js file.

.page.js will house all your selectors for your elements on your page. It can also hold some helper methods if you need them.
.spec.js is where you will write your tests.




##How to write your tests
First off you will need your .page file setup:
```Shell
var LoginPage = function () {
};

LoginPage.prototype = Object.create({}, {
  //your selectors live here
});

module.exports = LoginPage;
```

Now we need to make an instance of LoginPage within our /e2e/common/globals.js
```Shell
loginPage = function() {
  var LoginPage = require('../login/login.page');
  return new LoginPage();
};
```
Be sure to follow the naming conventions here.

Then we can create our .spec file.
Start it by including the globals.js file as we will need that for its helper methods and to create an instance of the LoginPage.

We then have our 'describe' block, which will contain all our tests which live within the 'it' blocks.
It's perfectly acceptable to have more than one describe block within your spec file,
it's useful when testing the same page in different situations or with different users.

Within the describe statement you need to get your environment ready to test the situation you want.
We can do this with a beforeAll statement. Within that you will need to do a number of things:
- Make instances of the classes your tests will need
- Call the browser to the page you need
- Call any helper functions to get started like disabling animations.

Then you are ready to write your 'it' statements which will contain your assertions.
Be sure to write these in a that tests can run as if they are the only 'it' statment within the describe, and that the 'it' statments can run one after another without failing.




##Running

To install the software run
```Shell
npm install && gulp bower
```

##Run some tests.
To run e2e tests only
```Shell
npm test:e2e
```

To run a particular suite of e2e tests
```Shell
//the 'login' suite
npm test:e2e:login
```

To run the code complexity reports
```Shell
//from https://github.com/es-analysis/plato#from-scripts
npm run complexity
```

To run an particular suite of tests
```Shell
npm run protractor:suitename
```
Make sure your suite is added to the protractor-conf.js and that you have added your script to run in the package.json





##Reports
When the Protractor scripts run, a xml output is generated at;
```Shell
  test/test-results/xmloutput.xml
```

But xml is boring to read, so we can use another tool which takes this xml and makes it somewhat readable;
```Shell
  https://github.com/Kenzitron/protractor-jasmine2-html-reporter
  npm install protractor-jasmine2-html-reporter --save-dev
```
Checkout the github page to see how this can all be configured. This project already has some settings set in the protractor-conf.js file.

//View reports
open reports/index.html




##Screengrabs
First you will need to configure the test which gathers the images
```Shell
  test/e2e/screengrabs/screengrabs/spec.js
```
After the before all is a piece of js code that runs through the screengrabs.json.js. It goes to the url's in the json, takes the screen shot and repeats.
Then if goes through the normal protractor format of using 'it' statments, where each statment is used to grab the image for each section it needs.

Once you have configured your scripts to gather your images and you know your file names, you can configure the 'responsive-screenshots.js' file.
This file is used to place the images into section onto your export file.

To generate screengrabs of the app
```Shell
  npm run protractor:screengrabs
```
In the package.json there is a post script which runs after the screengrabs is complete. It runs two scripts.
```Shell
node screenshots.js && node ./test/e2e/screengrabs/server/index.js ",
```




##Emailing screengrabs
Best place to look at documentation for this is the node module used to it
```Shell
https://github.com/nodemailer/nodemailer
```
To get the settings you will need please read
```Shell
https://support.google.com/a/answer/176600?hl=en
```
To email the screengrabs as a single png make sure you have the following enviroment variables set
```Shell
  EXPORT SMTP_EMAIL=youremailsmtpusername
  EXPORT SMTP_EPASSWORD=youremailsmtppassword
  EXPORT SCREENGRABS_EMAIL_RECIPIENTS=therecipient@someserver.com
```
Now when you run the e2e tests it will email the resulting screenshots to the SCREENGRABS_EMAIL_RECIPIENTS.
To run just the screengrabs use:
```Shell
  npm run protractor:screengrabs
```
The screengrabs will be output to the ./test/screenshots folder, a framed composite with will be created in the root folder
