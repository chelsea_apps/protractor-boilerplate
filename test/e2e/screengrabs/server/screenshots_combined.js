exports.config = { source: 'test/screenshots/framed/', paperSize: 'A1', screenShot: { width: 400, height: 700 } };
exports.screenshots = [{
    section: 'Login and Landing page',
    screens: [{
        filename: 'framed-login.png',
        'caption': 'Login page',
    }, {
        filename: 'framed-login-terms-of-use.png',
        'caption': 'Terms of use',
    }, {
        filename: 'framed-login-password.png',
        'caption': 'Password',
    }, {
        filename: 'framed-login-terms-and-conditions.png',
        'caption': 'Term and conditions',
    }, {
        filename: 'framed-save-user-id.png',
        'caption': 'Save user id',
    }, {
        filename: 'framed-landing-page.png',
        'caption': 'My pensions',
    }, {
        filename: 'framed-landing-page-my-savings.png',
        'caption': 'My savings',
    }, {
        filename: 'framed-choose-plan.png',
        'caption': 'Choose products',
    }]
}, {
    section: 'More menu',
    screens: [{
        filename: 'framed-more-menu.png',
        'caption': 'More menu',
    }, {
        filename: 'framed-more-menu-faq.png',
        'caption': 'My pensions',
    }, {
        filename: 'framed-more-menu-terms-and-privacy.png',
        'caption': 'Terma and privacy',
    }, {
        filename: 'framed-more-menu-about-this-app.png',
        'caption': 'About thisapp',
    }, {
        filename: 'framed-more-menu-contact-us.png',
        'caption': 'Contact us',
    }, {
        filename: 'framed-blog-time.png',
        'caption': 'Blog by time',
    }, {
        filename: 'framed-blog-time-blog-topic.png',
        'caption': 'Blog by topic',
    }, {
        filename: 'framed-blog-topic-blog-article.png',
        'caption': 'Blog article',
    }]
}, {
    section: 'Recent payments',
    screens: [{
        filename: 'framed-recent-payments.png',
        'caption': 'Recent payments',
    }, {
        filename: 'framed-recent-payments-filter.png',
        'caption': 'Filter',
    }, {
        filename: 'framed-recent-payments-current-tax-year.png',
        'caption': 'Current tax year',
    }, {
        filename: 'framed-recent-payments-last-12-months.png',
        'caption': 'Last 12 months',
    }, {
        filename: 'framed-recent-payments-custom-date-filter.png',
        'caption': 'Custom date filter',
    }, {
        filename: 'framed-recent-payments-custom-date-filter-populated.png',
        'caption': 'Date filter populated',
    }, {
        filename: 'framed-recent-payments-filtered-dates.png',
        'caption': 'Filtered dates',
    }]
}, {
    section: 'Investments and Fund detail',
    screens: [{
        filename: 'framed-investments.png',
        'caption': 'Investments',
    }, {
        filename: 'framed-investments-info.png',
        'caption': 'Investment info',
    }, {
        filename: 'framed-investments-floating-action-sheet.png',
        'caption': 'Investment actions',
    },{
        filename: 'framed-fund-detail.png',
        'caption': 'Fetail',
    }, {
        filename: 'framed-fund-detail-1-year.png',
        'caption': '1 year',
    }, {
        filename: 'framed-fund-detail-3-year.png',
        'caption': '3 year',
    }, {
        filename: 'framed-fund-detail-5-year.png',
        'caption': '5 year',
    }, {
        filename: 'framed-fund-detail-info.png',
        'caption': 'Info',
    }, {
        filename: 'framed-fund-detail-about-fund-performance.png',
        'caption': 'performance',
    }, ]
}, {
    section: 'Top up',
    screens: [{
        filename: 'framed-top-up.png',
        'caption': 'Top up',
    }, {
        filename: 'framed-topup-investment-details.png',
        'caption': 'Investment details',
    }, {
        filename: 'framed-top-up-info.png',
        'caption': 'Info',
    }, {
        filename: 'framed-topup-enterpayment-details.png',
        'caption': 'Enter payment',
    }, {
        filename: 'framed-topup-enterpayment-details-errors.png',
        'caption': 'Enter payment errors',
    }, {
        filename: 'framed-topup-payment-confirm.png',
        'caption': 'Confirm',
    }, {
        filename: 'framed-payment-submitted-success.png',
        'caption': 'Payment success',
    }, {
        filename: 'framed-payment-submitted-failed.png',
        'caption': 'Payment fail',
    }]
}, {
    section: 'Regular payment',
    screens: [{
        filename: 'framed-regular-payments.png',
        'caption': 'Regular payments',
    }, {
        filename: 'framed-regular-payment-confirm.png',
        'caption': 'Confirm',
    }, {
        filename: 'framed-regular-payments-direct-debit-and-investment-details.png',
        'caption': 'Investment details',
    }, {
        filename: 'framed-regular-payments-direct-debit-change-instructions.png',
        'caption': 'DD change instructions',
    }, {
        filename: 'framed-regular-payments-direct-debit-guarantee.png',
        'caption': 'DD guarantee',
    }, {
        filename: 'framed-payment-submitted-success.png',
        'caption': 'Payment success',
    }, {
        filename: 'framed-payment-submitted-failed.png',
        'caption': 'Payment fail',
    }]
}];
