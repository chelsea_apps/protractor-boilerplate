var express = require('express');
var app = express();
var exphbs = require('express-handlebars');
var screenshots = require('./screenshots_combined.js').screenshots;
var config = require('./screenshots_combined.js').config;
var git = require('git-rev-sync');
var nodemailer = require('nodemailer');
var Q = require('q');
var colors = require('colors');
var absorb = require('absorb');

config.currentDate = new Date();

//var branch = git.branch();

config.git = {
    short: git.short(),
    long: git.long(),
    message: git.message(),
    tag: git.tag(),
    branch: git.branch()
};

app.set('views', __dirname + '/templates/');
app.set('partials', __dirname + '/partials/');
app.set('extname', '.hbs');

app.engine('handlebars', exphbs({
    partialsDir: __dirname + '/partials/',
    extname: '.hbs',
    defaultLayout: 'main',
    layoutsDir: __dirname + '/layouts'
}));
app.set('view engine', 'handlebars');

app.use(express.static(__dirname + './../../../screenshots/framed'));
app.use(express.static(__dirname + '/css'));

app.get('/', function(req, res) {
    res.render('home', { config: config, screenshots: screenshots });
});

var server = app.listen(3000);
var phantom = require('phantom');
var screengrabFilename = 'screengrabs.png';

phantom.create().then(function(ph) {
    ph.createPage().then(function(page) {
        page.property('viewportSize', { width: 3200, height: 6600 }).then(function() {

            page.open('http://localhost:3000').then(function(status) {
                var str = 'Opening compilation web page... ' + status;
                console.log(str.rainbow);
                page.render(screengrabFilename, { format: 'png', quality: '70' }).then(function(content) {
                    console.log(colors.blue('Saving screengrab of web page to %s... success'), screengrabFilename);
                    page.close();
                    console.log('Page closed');
                    ph.exit();
                    console.log('Phantom exited');
                    var credentials = { smtpUsername: process.env.SMTP_USERNAME, smtpPassword: process.env.SMTP_PASSWORD, recipients: process.env.SCREENGRABS_EMAIL_RECIPIENTS };
                    console.log('Credentials set');
                    config = absorb(config, credentials);
                    console.log('Emailing...');
                    email(config).then(function(status) {
                        console.log(colors.inverse('Success - Email sent %s'), status);
                        server.close();
                    }, function(error) {
                        console.log('Email not sent: ');
                    });
                });

            });
        });

    });
});

function email(config) {

    var deferred = Q.defer()

    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://' + config.smtpUsername + ':' + config.smtpPassword);

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"Project Name CI 👥" <YOUREMAILADDRESS@googlemail.com>', // sender address
        to: config.recipients, // list of receivers
        subject: 'Screengrabs for ' + config.git.branch + ' branch ✔', // Subject line
        text: 'Screengrabs attached 🐴', // plaintext body
        html: '<dl><dt>Branch</dt><dd>' + config.git.branch + '</dd><dt>Git Hash</dt><dd>' + config.git.short + '</dd><dt>Git tag</dt><dd>' + config.git.tag + '</dd><dt>Latest commit message</dt><dd>' + config.git.message + '</dd></dl>', // html body
        attachments: [{ // utf-8 string as an attachment
            filename: 'screngrabs.png',
            path: 'screengrabs.png'
        }]
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log('oh no, an error'.rainbow);
            deferred.reject(error)
            return console.log(error);
        }
            console.log('YAY - email is fine'.rainbow);
        deferred.resolve(info.response);
    });

    return deferred.promise

}
