module.exports = {
    screens: [{
        url: '#/blog',
        filename: 'blog-topic',
        subpages: [{
            beforeClick: 'div.scroll>div>div:nth-of-type(1) ion-slides>div>div>div',
            filename: 'blog-article',
            afterClick: ''
        }]
    }, {
        url: '#/blog',
        filename: 'blog-topic',
        subpages: [{
            beforeClick: 'div.tab-nav a:nth-of-type(2)',
            filename: 'blog-time',
            afterClick: ''
        }]
    },{
        url: '/#/login',
        filename: 'login',
        subpages: [{
            beforeClick: '#login_termsOfUse',
            filename: 'terms-of-use',
            afterClick: ''
        }]
    }, {
        url: '/#/landing-page',
        filename: 'landing-page',
        subpages: [{
            beforeClick: 'div.swiper-pagination span:not(.swiper-pagination-bullet-active',
            filename: 'my-savings',
            afterClick: ''
        }]
    }, {
        url: '/#/chooseproduct/Pension/',
        filename: 'choose-plan'
    }, {
        url: '/#/fundlist/K1111111111/',
        filename: 'investments',
        subpages: [{
            beforeClick: 'ion-view[nav-view=active] div[nav-bar=active] button#fund_important_info',
            filename: 'info',
            afterClick: ''
        }]
    }, {
        url: '/#/funddetails/https:%252F%252Fsyst.standardlife.co.uk%252Fsecure%252Fmy-portfolio%252Frest%252Fsecured%252Ffund%3Fseries=4&code=KE/http:%252F%252Fwebfund6.financialexpress.net%252Fclients%252Fstandardlife%252FFundFactsheet.aspx%3Ftype=retail&code=SL28/16%252F03%252F2016/SL28/1567.93',
        filename: 'fund-detail',
        subpages: [{
            beforeClick: 'ion-view[nav-view=active]>ion-nav-bar>div[nav-bar=active]>ion-header-bar>div.buttons>span.right-buttons>button',
            filename: 'info',
            afterClick: 'div.modal-backdrop.active ion-header-bar>button#settings_close'
        }, {
            beforeClick: '#one_year_fundperformance',
            filename: '1-year',
            afterClick: ''
        }, {
            beforeClick: '#three_year_fundperformance',
            filename: '3-year',
            afterClick: ''
        }, {
            beforeClick: '#five_year_fundperformance',
            filename: '5-year',
            afterClick: ''
        }]
    }, {
        url: '#/fundperformanceinfo/06%252F03%252F2015/KWH3',
        filename: 'fund-detail-about-fund-performance'
    }, {
        url: '#/investmentdetails/K1111111111/Investment%20details',
        filename: 'topup-investment-details'
    }]
}
