'use strict';
var globals = require('../common/globals').globals;
var screengrabs = require('./screengrabs.json.js');
var login, landing, moreMenu, fundList, topUpPaymentPage, enterPaymentDetails, recentPayments, confirmPayment, increaseRegularPayments, directdebitsAndInvestments, investmentDetails;
login = loginPage();
landing = landingPage();
moreMenu = moreMenuPage();
fundList = fundListPage();
topUpPaymentPage = topUpPaymentsPage();
enterPaymentDetails = enterPaymentDetailsPage();
confirmPayment = confirmPaymentsPage();
increaseRegularPayments = increaseRegularPaymentsPage();
directdebitsAndInvestments = directDebitAndInvestmentsPage();
investmentDetails = investmentDetailsPage();
recentPayments = recentPaymentsPage();

describe('Screengrabs', function() {
    beforeAll(function(done) {
        browser.get('').then(function() {
            setDefaultUser('screengrab');
            browser.get('');
            disableCssAnimations();
            disableModalAnimations();
            disableAllAnimations().then(function() {
                done();
            });
        });
    });

    browser.driver.manage().window().setSize(360, 700);
    screengrabs.screens.forEach(function(screen) {
        it('Should capture ' + screen.filename, function(done) {
            browser.get(screen.url).then(function() {
                if (screen.filename) {
                    screengrab(screen.filename);
                }
                if (screen.subpages) {
                    screen.subpages.forEach(function(subscreen) {
                        if (subscreen.beforeClick) {
                            var toClick = element(by.css(subscreen.beforeClick));
                            toClick.click();
                        }
                        browser.sleep(1000);
                        if (subscreen.filename) {
                            screengrab(screen.filename + '-' + subscreen.filename);
                        }
                        if (subscreen.afterClick) {
                            var toClick = element(by.css(subscreen.afterClick));
                            toClick.click();
                        }
                    });
                    done();
                } else {
                    done();
                }
            })

        });
    });

    //Login Terms and conditions
    it('Should capture password and terms and conditions', function(done) {
        browser.get('/')
        login.userId.sendKeys('screengrabs');
        login.userId.sendKeys(protractor.Key.ENTER);

        login.sparsePassword1.sendKeys('1');
        screengrab('login-password');
        login.sparsePassword2.sendKeys('1');
        login.sparsePassword3.sendKeys('1').then(function() {
            browser.sleep(5000);
            screengrab('login-terms-and-conditions');
            screengrab('save-user-id');
            done();
        });
    });

    //Side Menu
    it('Should capture the side menu and its pages', function(done) {
        browser.get(globals.baseUrl + landing.url);
        landing.openMoreMenu().then(function() {
            expect(landing.moreMenuScreen.isDisplayed()).toBeTruthy();
            screengrab('more-menu');
            done();
        });
        moreMenu.faqs.click().then(function() {
            screengrab('more-menu-faq');
            jsClick('settings_close');
            done();
        })
        moreMenu.termsAndPrivacy.click().then(function() {
            screengrab('more-menu-terms-and-privacy');
            jsClick('settings_close');
            done();
        });
        moreMenu.aboutThisApp.click().then(function() {
            screengrab('more-menu-about-this-app');
            jsClick('settings_close');
            done();
        });
        moreMenu.contactUs.click().then(function() {
            screengrab('more-menu-contact-us');
            jsClick('settings_close');
            done();
        });
    });
    //Open FAB button menu
    it('Should capture Floating Action Sheet', function(done) {
        browser.get(globals.baseUrl + '#/fundlist/K1111111111/');
        expect(fundList.fab.isDisplayed()).toBeTruthy();
        fundList.fab.click();
        expect(fundList.actionSheet.isDisplayed()).toBeTruthy();
        screengrab('investments-floating-action-sheet');
        done();
    });

    //continue on from opening FAB to Increase regular payments
    //Enter Regular payment amount
    it('Should capture confirm regular payment', function(done) {
        fundList.regularPaymentsButton.click().then(function() {
            screengrab('regular-payments');
            increaseRegularPayments.directDebitsAndInvestmentDetails.click()
            screengrab('regular-payments-direct-debit-and-investment-details');
            directdebitsAndInvestments.changeInvestmentInstructionsButton.click();
            screengrab('regular-payments-direct-debit-change-instructions');
            directdebitsAndInvestments.cancelButton.click();
            directdebitsAndInvestments.directDebitGauranteeLink.click();
            screengrab('regular-payments-direct-debit-guarantee');
            done();
        });
    });

    //Enter topup amount
    it('Should capture enter payment details, and confirm top up', function(done) {
        browser.get(globals.baseUrl + '#/fundlist/K1111111111/');
        expect(fundList.fab.isDisplayed()).toBeTruthy();
        fundList.fab.click();
        expect(fundList.actionSheet.isDisplayed()).toBeTruthy();
        fundList.topupButton.click().then(function() {
            screengrab('top-up');
            topUpPaymentPage.informationButton.click();
            screengrab('top-up-info');
            topUpPaymentPage.infoBackbutton.click();
            topUpPaymentPage.viewInvestmentDetailsLink.click();
            screengrab('top-up-investment-details');
            investmentDetails.changeInvestmentInstructionsButton.click()
            screengrab('top-up-investment-details-change-instructions');
            done();
        });
    });

    //Regular payments
    it('Should capture confirm regular payment', function(done) {
        browser.get(globals.baseUrl + '#/fundlist/K1111111111/');
        expect(fundList.fab.isDisplayed()).toBeTruthy();
        fundList.fab.click();
        expect(fundList.actionSheet.isDisplayed()).toBeTruthy();
        fundList.regularPaymentsButton.click()
        increaseRegularPayments.enterPaymentAmountAs('415');
        increaseRegularPayments.clickNextButton.then(function() {
            screengrab('regular-payment-confirm');
            confirmPayment.termsandConditionsLink.click();
            screengrab('regular-payment-confirm-terms-and-conditions');
            confirmPayment.closeTermsAndConditionsModal.click();
            scrollBottom();
            confirmPayment.confirmButton.click();
            screengrab('payment-submitted-failed');
            done();
        });
    });

    //Top up
    it('Should capture enter payment details, and confirm top up', function(done) {
        browser.get(globals.baseUrl + '#/fundlist/X3333333333/');
        expect(fundList.fab.isDisplayed()).toBeTruthy();
        fundList.fab.click();
        expect(fundList.actionSheet.isDisplayed()).toBeTruthy();
        fundList.topupButton.click();
        topUpPaymentPage.enterPaymentAmountAs(500);
        topUpPaymentPage.nextButton.click();
        screengrab('topup-enterpayment-details');
        enterPaymentDetails.clickNextButton.then(function() {
            screengrab('topup-enterpayment-details-errors');
            enterPaymentDetails.enterAllDetails();
            screengrab('topup-payment-confirm');
            confirmPayment.termsandConditionsLink.click();
            screengrab('top-up-confirm-terms-and-conditions');
            confirmPayment.closeTermsAndConditionsModal.click();
            scrollBottom();
            confirmPayment.confirmButton.click();
            screengrab('payment-submitted-success');
            done();
        });
    });

    it('Should capture recent payments and filters', function(done) {
        browser.get(globals.baseUrl + '#/fundlist/K1111111111/');
        scrollBottom();
        fundList.recentPayments.click().then(function() {
            screengrab('recent-payments');
            recentPayments.toggleFilter.click();
            screengrab('recent-payments-filter');
            recentPayments.currentTaxYear.click();
            screengrab('recent-payments-current-tax-year');
            recentPayments.toggleFilter.click();
            recentPayments.last12Months.click();
            screengrab('recent-payments-last-12-months');
            recentPayments.toggleFilter.click();
            recentPayments.custom.click();
            screengrab('recent-payments-custom-date-filter');
            recentPayments.fromDate.sendKeys('10/09/2014');
            recentPayments.toDate.sendKeys('23/03/2015');
            screengrab('recent-payments-custom-date-filter-populated');
            recentPayments.applyFilter.click();
            screengrab('recent-payments-filtered-dates');
            done();
        });
    });
    afterEach(function() {
        browser.executeScript('window.localStorage.clear();');
    });
    afterAll(function() {
        setDefaultUser('TESTER');
    });
});
