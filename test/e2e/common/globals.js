function applicationWait(time) {
  if (time != null && time != undefined) {
    browser.driver.sleep(time);
  } else {
    browser.driver.sleep(1000);
  }
}


disableCssAnimations = function() {
  return browser.executeScript("document.body.className += ' notransition';");
};

disableModalAnimations = function() {

  var style = 'var cssText = ".spinner-container {z-index:-999; height: 0; width:0;display:none !important}; .card, .animated, .am-fade-and-scale {animation: none; animation-delay: 0 !important; -webkit-animation-duration: 0ms !important; animation-duration: 0ms !important}"; ' +
    'var css = document.createElement("style");' +
    'css.type = "text/css";' +
    'if("textContent" in css) {' +
    'css.textContent = cssText;}' +
    'else {' +
    'css.innerText = cssText}' +
    'document.body.appendChild(css);'
  return browser.executeScript(style);
}

disableAllAnimations = function() {

  var style = 'var cssText = ".card, .animated, .am-fade-and-scale {animation: none; animation-delay: 0 !important; -webkit-animation-duration: 0ms !important; animation-duration: 0ms !important}"; ' +
    'var css = document.createElement("style");' +
    'css.type = "text/css";' +
    'if("textContent" in css) {' +
    'css.textContent = cssText;}' +
    'else {' +
    'css.innerText = cssText}' +
    'document.body.appendChild(css);'
  return browser.executeScript(style);
}

populateDateField = function(date, id) {
  var setDate = '' +
    'var theDate = new Date("' + date + '");' +
    'var scope = angular.element(document.getElementById("' + id + '")).scope();' +
    'scope.debitCard.' + id + ' = theDate;' +
    'scope.$apply();';
  return browser.executeScript(setDate);
};

scrollBottom = function() {
  //return browser.executeScript('window.scrollTo(0,0);');
  return browser.executeScript('window.$ionicScrollDelegate.scrollBottom();');
};
scrollTop = function() {
  return browser.executeScript('window.$ionicScrollDelegate.scrollTop();');
};
scrollTo = function(left, top) {
  var scrollScript = 'window.$ionicScrollDelegate.scrollTo(' + left + ',' + top + ');';
  return browser.executeScript(scrollScript);
};
jsClick = function(id) {
  return browser.executeScript('document.getElementById(\''+ id +'\').click();');
};
setDefaultUser = function(name) {
  var user = 'window.name = \''+name+'\'';
  return browser.executeScript(user);
};

//screenshot code
var fs = require('fs');
var screenShotDirectory = 'test/screenshots';
fs.existsSync(screenShotDirectory) || fs.mkdirSync(screenShotDirectory);
// abstract writing screen shot to a file
writeScreenShot = function(data, filename) {
  var stream = fs.createWriteStream(screenShotDirectory + '/' + filename + '.png');

  stream.write(new Buffer(data, 'base64'));
  stream.end();
};
screengrab = function(filename) {
  browser.sleep(500);
  browser.takeScreenshot().then(function(png) {
    writeScreenShot(png, filename);
  });
};

//For any other pages, duplicate this loginPage method for other page objects.
loginPage = function() {
  var LoginPage = require('../login/login.page');
  return new LoginPage();
};


exports.globals = {
  'baseUrl': 'http://localhost:8101/',
  'screenshotsLocation': 'screenshots',
  'logo': {
    get: function() {
      return element(by.css('.header-item'));
    }
  },
  'writeScreenShot': writeScreenShot
};