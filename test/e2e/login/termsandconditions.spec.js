var globals = require('../common/globals').globals;
var termsAndConditions;

//If a user has not logged in to the application before, they should be
//presented with the terms and conditions page after logging in.
//This is determined in the config.json termsandconditions true ? false.
describe('Terms and conditions.', function() {
    beforeAll(function(done) {
        termsAndConditions = loginPage();
        browser.get('').then(function(){
            setDefaultUser('screengrab');
            browser.get('');
            disableCssAnimations();
            disableModalAnimations();
            disableAllAnimations().then(function() {
            termsAndConditions.loginAs('screengrab');
                done();
            });
        });
    });

    it('should display terms and conditions view.', function() {
        expect(termsAndConditions.termsAndConditions.isDisplayed()).toBeTruthy();
    });
    
    it('should display the subheading \'Please agree to our Terms & Conditions to continue\'', function() {
        expect(termsAndConditions.subHeading.isDisplayed()).toBeTruthy();
        expect(termsAndConditions.subHeading.getText()).toEqual('Please agree to our Terms & Conditions to continue');
    });
    
    it('should display copyright information at the bottom of the view.', function() {
        scrollBottom();
        expect(termsAndConditions.copyright.isDisplayed()).toBeTruthy();
        expect(termsAndConditions.copyright.getText()).toEqual('© 2016 Standard Life, images reproduced under licence')
    });

    it('should display Disagree and Agree buttons at the bottom of the page.', function() {
        expect(termsAndConditions.agreeButton.isDisplayed()).toBeTruthy();
        expect(termsAndConditions.disagreeButton.isDisplayed()).toBeTruthy();
    });
    
    it('should navigate back to login page on click of Disagree button.', function() {
        jsClick('disagreeTermsAndConditions');
        expect(termsAndConditions.userId.isDisplayed()).toBeTruthy();
    });

    afterEach(function () {
        browser.executeScript('window.localStorage.clear();');
    });

    afterAll(function () {
        setDefaultUser('TESTER');
    });
});

describe('Agreeing with Terms and conditions.', function() {
    beforeAll(function(done) {
        termsAndConditions = loginPage();
        browser.get('').then(function(){
            setDefaultUser('screengrab');
            browser.get('');
            disableCssAnimations();
            disableModalAnimations();
            disableAllAnimations().then(function() {
            termsAndConditions.loginAs('screengrab');
                done();
            });
        });
    });
    
    it('should display a pop up asking weather the user would like to save their UserrId, after clicking the Agree button.', function() {
        termsAndConditions.agreeButtonClick();
        expect(termsAndConditions.saveUserId.isDisplayed()).toBeTruthy();
    });

    afterEach(function () {
        browser.executeScript('window.localStorage.clear();');
    });

    afterAll(function () {
        setDefaultUser('TESTER');
    });
});

