var globals = require('../common/globals').globals;
var login, landing;

describe('Login page.', function() {
    beforeAll(function(done) {
        login = loginPage();
        landing = landingPage();
        browser.get(globals.baseUrl).then(function(){
            browser.executeScript('window.localStorage.clear();').then(function(){
                disableCssAnimations();
                disableModalAnimations();
                disableAllAnimations().then(function() {
                    done();
                });
            });
        });
    });
    
    it('should have the correct URL', function() {
    	expect(browser.getCurrentUrl()).toEqual('http://localhost:8101/#/login');
	});
    it('should have a link to the Forgotten login details page. This should open in the external browser.', function() {
        expect(login.forgotDetails.isDisplayed()).toBeTruthy();
    });
    it('should display the SL logo at the top of the view.', function() {
        expect(login.logo.isDisplayed()).toBeTruthy();
    });
    it('should display the into text underneath the logo.', function() {
        expect(login.intro.isDisplayed()).toBeTruthy();
    });
    it('should have a link to Register (external browser) and Terms of use pages (modal view)', function() {
    	expect(login.registerHere.isDisplayed()).toBeTruthy();
    	expect(login.termsOfUse.isDisplayed()).toBeTruthy();
    });
    it('should successfully login to the application and display the Landing page.', function() {
        login.loginAs('tester');
        expect(landing.spinner.isDisplayed()).toBeTruthy();
    });

    afterEach(function () {
        browser.executeScript('window.localStorage.clear();');
    });
});

//If the user has their User ID saved, when they launch the application,
//they should be taken directly to the password page, to login straight away.
//Variables for these can be found in Local Storage
//username: 'tester'
//usernamepref: true
describe('Login page, straight to Password page with a saved User ID.', function() {

    beforeAll(function(done) {
        landing = landingPage();
        login = loginPage(); 
        browser.get(globals.baseUrl).then(function(){
            disableCssAnimations();
            disableModalAnimations();
            disableAllAnimations().then(function() {
                login.loginAs('tester');
                browser.get('');
                done();
            });
        });
    });

    it('should display the password page with the User Id field filled with the users ID.', function() {
        expect(login.userId.isDisplayed()).toBeTruthy();
    }).pend('needs to be tested manually. Can not set saved user before opening the browser.');

    it('should have focus on the first password field.', function() {
        expect(login.sparsePassword1.getAttribute('ng-model')).toEqual(browser.driver.switchTo().activeElement().getAttribute('ng-model'));
    }).pend('needs to be tested manually. Can not set saved user before opening the browser.');

    it('should display the Landing page when all 3 correct password fields are entered.', function(done) {
        login.sparsePassword1.sendKeys('1');
        login.sparsePassword2.sendKeys('1');
        login.sparsePassword3.sendKeys('1').then(function(){
            login.agreeButtonClick();
            expect(landing.spinner.isDisplayed()).toBeTruthy();
            done();
        });
    }).pend('needs to be tested manually. Can not set saved user before opening the browser.');

    afterAll(function () {
        browser.executeScript('window.localStorage.clear();');
    });
});


//If a user has not logged in to the application before, they should be
//presented with the terms and conditions page after logging in.
//This is determined in the config.json termsandconditions true ? false.
describe('Login page, with terms and conditions', function() {
    beforeAll(function(done) {
        login = loginPage();
        browser.get('').then(function(){
          setDefaultUser('screengrab');
          browser.get('');
          disableCssAnimations();
          disableModalAnimations();
          disableAllAnimations().then(function() {
                done();
            });
        });
    });

    it('should display the terms and conditions page when logging into the for the first ever time', function() {
        login.loginAs('screengrab');
        expect(login.termsAndConditions.isDisplayed()).toBeTruthy();
    });

    afterAll(function () {
        browser.executeScript('window.localStorage.clear();');
        setDefaultUser('TESTER');
    });
});
