'use strict';

var globals = require('../common/globals').globals;
var users = require('../common/users').users;

var LoginPage = function () {
};

LoginPage.prototype = Object.create({}, {

//buttons
  loginButton: { get: function () { return element(by.id('login_button')); }},
  nextButton: { get: function () { return element(by.id('next_button')); }},
  loginButtonDisabled: { get: function () { return element(by.css('#login_button.disabled')); }},
  loginButtonActive: { get: function () { return element(by.css('#login_button.active')); }},
  nextButtonDisabled: { get: function () { return element(by.css('#next_button.disabled')); }},
  disagreeButton: { get: function () { return element(by.id('disagreeTermsAndConditions')); }},
  agreeButton: { get: function () { return element(by.id('agreeTermsAndConditions')); }},
  saveUserId: { get: function () { return element(by.buttonText("Yes")); }},
  popupRetry: { get: function () { return element(by.buttonText("Retry")); }},
  okButton: { get:function() { return element.all(by.repeater('button in buttons')).first();}},

  //inputs
  userId: { get: function () { return element(by.model('loginData.username')); }},
  sparsePassword1: { get: function () { return element(by.model('loginData.sparseAnswers[0]')); }},
  sparsePassword2: { get: function () { return element(by.model('loginData.sparseAnswers[1]')); }},
  sparsePassword3: { get: function () { return element(by.model('loginData.sparseAnswers[2]')); }},

  //links
  registerHere: { get: function () { return element(by.id('login_registerHere')); }},
  termsOfUse: { get: function () { return element(by.id('login_termsOfUse')); }},
  forgotDetails: { get: function () { return element(by.css('.forgot-details')); }},
  termsAndConditions: { get: function () { return element(by.css('.terms-and-conditions.modal')); }},

  //boilerplate
  logo: { get: function () { return element(by.id('login_header')); }},
  intro: { get: function () { return element(by.xpath('//img[@id="login_header"]/../p[@class="login-intro"]')); }},
  passwordHelp: { get: function () { return element(by.css("p.login-intro")); }},

  //strings
  passwordDescription: { get: function () { return "Please enter the following\ncharacters from your password" }},
  subHeading: {get: function (){return element(by.css('div.modal-backdrop.active div.terms-of-use h3:nth-of-type(1)')); }},
  copyright: {get: function (){return element(by.css('div.modal-backdrop.active div.terms-of-use p:last-of-type')); }},
  incorrectUserIdTitle: {get: function (){ return element(by.css('.popup-title'));}},
  
  enterUserId: { value: function () {
    this.userId.sendKeys(users.tester.id);
    this.userId.sendKeys(protractor.Key.ENTER);
  }},
  enterPassword: { value: function () {
    this.sparsePassword1.sendKeys(users.tester.password.substr(0, 1));
    this.sparsePassword2.sendKeys(users.tester.password.substr(1, 1));
    this.sparsePassword3.sendKeys(users.tester.password.substr(2, 1));
  }},

  //actions
  fullLoginAs: { value: function (user) {
    this.login(users[user].id,users[user].password);
    this.agreeButtonClick();
    this.saveUserId.click();
  }},
  loginAs: { value: function (user) { this.login(users[user].id,users[user].password); }},
  login: { value: function (username, password) {
    this.userId.sendKeys(username);
    this.userId.sendKeys(protractor.Key.ENTER).then(function(){
      this.sparsePassword1.sendKeys(password.substr(0, 1));
      this.sparsePassword2.sendKeys(password.substr(1, 1));
      this.sparsePassword3.sendKeys(password.substr(2, 1));
      browser.actions().sendKeys(protractor.Key.ENTER).perform();
    }.apply(this));
  }},

  agreeButtonClick: { value: function() {
    jsClick('agreeTermsAndConditions');
  }},

  setLoginPreferences: { value: function() {
    browser.executeScript('window.localStorage.setItem("username","tester");');
    browser.executeScript('window.localStorage.setItem("usernamepref","false");');
  }}
});

module.exports = LoginPage;
